Electrumx server docker image
=============================

Docker image of [electrumx][] server.

[electrumx]: https://github.com/kyuupichan/electrumx


Locations
---------

Source of the image is hosted on Bitbucket at
https://bitbucket.org/beli-sk/docker-electrumx

If you find any problems, please post an issue at
https://bitbucket.org/beli-sk/docker-electrumx/issues

The built image is located on Docker Hub at
https://hub.docker.com/r/beli/electrumx/


Pull or build
-------------

The image is built automatically on Docker hub in repository **beli/electrumx**
and can be pulled using command

    docker pull beli/electrumx

or if you'd prefer to build it yourself from the source repository

    git clone https://bitbucket.org/beli-sk/docker-electrumx.git
    cd docker-electrumx/
    docker build -t beli/electrumx .


Usage
-----

The container uses volume `/srv/db` for the database.  The volumes
should be owned and writable by UID 1000, GID 1000.

    docker run -d beli/electrumx


Credits
-------

Inspired by https://github.com/nyanloutre/docker-electrumx

